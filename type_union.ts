//Union type คือตัวที่จะทำให้เราสามารถรับ type มากกว่า 1 type เข้ามาทำงานได้ 
function printStatusCode(code: string | number) {
    if (typeof code === 'string') {
        console.log(`My status code is ${code.toUpperCase()} ${typeof code}.`)
    } else {
        console.log(`My status code is ${code} ${typeof code}.`)
    }
}
printStatusCode(404);
printStatusCode('abc');