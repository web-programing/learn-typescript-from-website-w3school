function getTime(): number {
    return new Date().getTime();
}

console.log(getTime());

function printHello(): void {
    console.log("Hello");
}

printHello();

// function  ที่ส่ง parameter เข้าไปได้
function multiple(a: number, b:number):number { //inference type
    return a * b;
}

console.log(multiple(1,2));

// function Optional
function add(a: number, b:number, c?:number):number {
    return a + b + (c || 0); // (c || 0) คือถ้า c เป็น undefined(null) ให้ c มีค่าเป็น 0
}

console.log(add(1,2,3));
console.log(add(1,2));

//การใส่ defalut parameter 
function pow(value: number, exponent: number = 10):number { //pow(ตัวเลขที่รับเข้ามา, ยกกำลัง)
    return value ** exponent; //ยกกำลัง ใช้ **
}

//ถ้าส่งค่าเข้าไปตัวเดียวจะยกกำลังตามเรากำหนดเอาไว้
console.log(pow(5));
//วิธีการส่งค่าและค่ายกกำลัง
console.log(pow(5,2));

//Name parameter คือ ตัวมันเองสามารถมีชื่อมาให้กำหนดได้
function divide({ dividend, divisor} : { dividend: number, divisor: number}) {// { dividend, divisor} <-- ตัวแปร : { dividend: number, divisor: number} <-- นิยามว่าตัวแปรมี type เป็นแบบใด
    return dividend / divisor;
}

console.log(divide({dividend: 6, divisor: 2}));

//Rest Parameters
function add2(a: number, b: number, ...rest: number[]) { // ...rest คือการรับค่าเข้ามาใน array ที่ชื่อว่า rest ได้เรื่อยๆ
    return a + b + rest.reduce((p, c) => p + c, 0); // วน + ค่าทั้งหมดตั้งแต่ค่าตั้งต้น ( 0 ) จนครบค่าที่เข้ามาใหม่ทั้งหมด
    // reduce(ย่อ หรือ ลด)
    // p คือค่าตั้งต้น
    // c คิอค่าที่เข้ามาใหม่
    // 0 คือค่าตั้งต้น
  }

console.log(add2(1,2,3,4,5)); // add2(10 คือ ,10 คือ, ตั้งแต่ 10,10,ถึง10 คือการนำค่าทั้งหมดไปเก็บใน array ที่ชื่อว่า rest)

//Type Alias
type Negate = (value: number) => number;

//Arrow function =>
const negateFunction: Negate = (value: number) => value * -1;  //=> value * -1 คือการ return

//Normal
const negateFunction2: Negate = function(value: number):number {
    return value * -1
};

console.log(negateFunction(1));
console.log(negateFunction2(1));

