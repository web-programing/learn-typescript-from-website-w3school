// Index Signatures
const nameAgeMap: {[index: string]: number} = {}; // เราสามารถกำหนด index ได้เอง แต่รับ number เข้ามา
nameAgeMap.Jack = 25;
nameAgeMap.Kob = 44;
nameAgeMap.Mark = 50;
console.log(nameAgeMap);