type CarYear = number;
type CarType = string;
type CarModel = string;

type Car = { //Car เป็น class คือสามารถกำหนด type ได้
    year: CarYear,
    type: CarType,
    model: CarModel
}

const carYear: CarYear = 2001;
const carType: CarType = "Toyota"; 
const carModel: CarModel = "Corolla";

// กำหนด Type เป็นแบบ Object
const car1: Car = {
    year: carYear,
    type: carType,
    model: carModel
}

console.log(car1);