//TypeScript Casting คิือ การเปลี่ยน ชนิดของตัวแปร
let x: unknown = "hello";
console.log((<string> x).length);
 
//Force casting คือ การแปลงสิ่งที่กำหนดชนิดของตัวแปรมาแล้วเป็นอีกชนิดของตัวแปรที่เราต้องการ
let num: string = "1";
console.log(((num as unknown) as number));