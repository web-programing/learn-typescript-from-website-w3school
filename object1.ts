/* การสร้าง Object แบบง่ายๆ
const obj = {

};
*/
const car: {type: string, model: string, year?: number} = {
    type: "Toyota",
    model: "Corolla"
    
};

console.log(car);