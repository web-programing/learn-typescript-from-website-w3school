let w: unknown = 1;
w = "string";
w = { // การสร้าง Object
    runANonExistentMethod: () => { // การเขียน Method แบบ Arrow Method
        console.log("I think therefore I am");
    } // การทำ as คือการบอกว่า Object ด้านบนมี type เป็บแบบใน as
} as { runANonExistentMethod: () => void }  //การ defind ตัว Interface ขึ้นมา (defind object)

if(typeof w === 'object' && w !== null) { // เช็คว่า w เป็น Object ไหม และ w ไม่เท่ากับ null ใช่ไหม
    (w as { runANonExistentMethod: () => void }).runANonExistentMethod(); // การเปลี่ยน object w มีหน้าตาเป็นแบบ { runANonExistentMethod: () => void }
}