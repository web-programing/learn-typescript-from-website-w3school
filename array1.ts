// แบบกำหนด  Type
const names: string[] = [];
names.push("Dylan");
names.push("Sukdituch");
console.log(names[0]);
console.log(names[1]);
console.log(names.length);
// การวนแสดงค่า
// วิธีที่1
for(let i=0; i<names.length;i++){
    console.log(names[i]);
}
// วิธีที่2
for(let ind in names){
    console.log(names[ind]);
}
// วิธีที่3
names.forEach(function(name){ //เป็น Anonymous Function
    console.log(name);
});