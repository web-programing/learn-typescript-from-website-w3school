// interface มีความบางกว่า class / Object ไม่สามารถเกิดขึ้นได้ / แต่ใน javascript object สามารถเกิดขึ้นได้ตลอดเวลา
interface Rectangle {
    height: number,
    width: number
}


// Extends interface
interface ColorRectangle extends Rectangle {
    color: string
}

const rectangle: Rectangle = {
    width: 20,
    height: 10
}
console.log(rectangle)

const ColorRectangle: ColorRectangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(ColorRectangle);

// เราไม่สามารถสร้าง typeได้ แต่เราสามารถกำหนด  type ได้